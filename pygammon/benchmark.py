import cProfile
import board, constants, movegenerator
import time

def test_get_legal_moves():
    b = board.Board()
    b.new_game(constants.white)
    results = list(movegenerator.get_legal_moves(b))
    print(f"Numbers of moves {len(results)}")
    
cProfile.run('test_get_legal_moves()')
