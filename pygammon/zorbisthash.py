import random

def _get_random_strings(count):
    """"Sets up random strings for our zorbist hash"""    
    return tuple(random.getrandbits(64) for i in range(count))
        
def _get_random_strings_for_points(count):
    return tuple(_get_random_strings(15) for i in range(count))

class ZorbistHash:
    """Class implementing backgammon specific zorbist hashing"""
    #bit_strings[color][point][number_of_stones]
    #point constants.bar is stones on the bar

    bit_strings = (_get_random_strings_for_points(25), _get_random_strings_for_points(25))
    
    @property
    def hash(self):
        return self._hash
        
    def __init__(self, hash = 0):
        """instantiates a new zorbist hash, optionally passing an existing hash"""
        self._hash = hash

    def set(self,color,point, count):
        """Sets (or unsets) the state of the zorbist hash, uses constants.bar for the bar"""
        self._hash = self._hash ^ ZorbistHash.bit_strings[color][point][count]

    def __str__(self):
        return str(self._hash)


