#For help with rules see http://www.bkgm.com/rules.html
import constants 
import iteration.tools as query
import itertools

#Back gammon board
#This class should not contain AI functions such as generating all legal moves, or board evaluation
class Board:
    """The board class encapsulates the board state and provides methods to manipulate that state inline with the rules of backgammon"""
    
    __slots__ = ['points', 'bar', 'all_home', 'color', 'last_move']
    
    def __init__(self, board = None):
        if board != None:
            self._init_from_board(board)
        else:
            self._init_new()

    def _init_new(self):
        points = tuple([None,0] for i in range(24))
        self.points = (points, points[::-1])
        self.bar = [0, 0]                               #Count of stones on each player's bar
        self.all_home = [False, False]                  #True if a player has all their stones on home
        self.color=None                                 #which player is to move
        self.last_move = None
    
    def _init_from_board(self,board):
        points = tuple(
                        [board.points[constants.white][i][constants.point_color],
                        board.points[constants.white][i][constants.point_count]]  
                        for i in range(24))
        self.points = (points, points[::-1])
        self.bar = [board.bar[0], board.bar[1]]                               #Count of stones on each player's bar
        self.all_home = [board.all_home[0], board.all_home[1]]                 #True if a player has all their stones on home
        self.color=board.color                                 #which player is to move
        self.last_move = board.last_move
        
    
    def new_game(self, color):
        """initializes the board to a new game state, with player color going first"""
        self.color = color
        for c in constants.colors:
            self.set_point(c,23,c,2)
            self.set_point(c,12,c,5)
            self.set_point(c,7,c,3)
            self.set_point(c,5,c,5)

        self.__pre_calculate()

    @property
    def my_all_home(self):
        """returns whether current player has all stones on home"""
        return self.all_home[self.color]

    @property
    def my_points(self):
        """returns a points array for the current player"""
        return self.points[self.color]
        
    @property
    def my_bar(self):
        """returns count of stones on the bar for the current player"""
        return self.bar[self.color]
        
    @my_bar.setter
    def my_bar(self,value):
        """Sets the count of stones on the bar for the current player"""
        self.bar[self.color] = value

    def play_move(self, move):
        """Plays a move on the board"""
        if move.color != self.color: raise ValueError(f"Illegal move: It's not {move.color} move") 
        #todo: If both move are a pass, ensure there is no legal move that could be played
        #todo: If one move is a pass, ensure there is no possible way two move could be played instead.
        
        for half_move in move.half_moves:
            self.__play_half_move(half_move)
        
        self.last_move = move
    
    def setup_board(self,color,points,bar = [0,0]):
        """Allows you to define a custom board starting state
        point.Points: array of 24 point.Points object, numbered from white's perspective
        Bar: Array of stones on the bar, white, black
        Color: Whose turn it is to play next"""
        
        if len(points) != 24: raise ValueError("Must have 24 points")
        if len(bar) != 2: raise ValueError("Must have 2 bars")

        self.points = (list(points), points[::-1])  #clone list
        self.bar = bar
        self.color = color
        
        self.__pre_calculate()    #update any state, calc board hash.

    def __play_half_move(self, half_move):
        """This function plays one roll of two rolls of a full two-roll move"""
        if half_move.point == constants.move_pass:
            return
        
        #If there is any of my stones on the bar, it's mandatory to play them
        if self.my_bar > 0:
            if half_move.point != constants.bar: raise ValueError("Move type has to be constants.bar!")
            self.__play_enter(half_move.roll)
            return
        
        #Test if we are bearing off
        #We are bearing if all our stones are on home and the roll is high enough to move the point off the board
        
        if self.my_all_home and half_move.point<= half_move.roll -1:
           self.__play_bear(half_move)
           return
    
        source_point = self.my_points[half_move.point]
        
        if source_point[constants.point_color] == None or source_point[constants.point_count] == 0:
            raise ValueError("You can't play a move from a point with no stones on it!")
    
        #Move is a normal move
        self.__play_stone(half_move)
        
        self.__pre_calculate()  #Update any precalculated state

    def __play_bear(self,half_move):
        points = self.my_points
        roll_point = half_move.roll -1
        #the player is permitted (and required) to remove a stone from the highest point on which one of his stones resides
        highest_point =query.find_last_index(lambda p: p[constants.point_color] == self.color,itertools.islice(points,half_move.roll))
        if half_move.point == highest_point:
            #valid, remove stone
            self.remove_my_stone(half_move.point)
        else:
            raise ValueError(f"Illegal mbove -- you must bear off the highest point ({highest_point}) possible with your roll")

    def __play_enter(self, roll):
        """Takes a stone from color's bar and plays it on point, if legal to do so"""
        if self.my_bar == 0: raise ValueError(f"There is no stone on {self.color}'s bar!")
        #stones from the bar are always played at the dice roll's point
        self.__place_stone(23 - (roll - 1))
        #Remove played stone from the bar
        self.my_bar -= 1

    def __play_stone(self, half_move):
        """Plays a normal (non bar, non bearing, move )"""
        source_point= half_move.point
        dest_point = half_move.point - half_move.roll
        self.__place_stone(dest_point)
        self.remove_my_stone(source_point)
    
    #todo: Be nice to move this into the point.Point class itself
    def __place_stone(self, point):
        """Plays a stone of the given color at the given point, implementing game rules
        Will capture an enemy stone if one exists and place it on the bar"""
        p = self.my_points[point]
        if p[constants.point_color] == None:                  #if point is empty you can always occupy
            self.set_my_point(point,self.color,1)
        elif p[constants.point_color] == self.color:               #if point is owned by you, you can always occupy
            self.add_my_stone(point)
        elif p[constants.point_count] == 1:                   #if point is not owned by you, and contains 1 enemy, you can capture
            self.bar[p[constants.point_color]] += 1   #increment enemy bar
            self.set_my_point(point,self.color,1) #swap color
        else:
            raise ValueError("Illegal placement, point contains more than on enemy stone")
            


    def initialize_board(self):
        self.__pre_calculate()

    def __pre_calculate(self):
        """Calculates any pre-calculated board state"""
        #Check if all of white or black stones are in the home positions (0,1,2,3,4,5)
        self.all_home[constants.white] = all(p[constants.point_color] != constants.white for p in self.points[constants.white][6:])
        self.all_home[constants.black] = all(p[constants.point_color] != constants.black for p in self.points[constants.black][6:])

    def __str__(self):
        for i in range(24):
            print(self.points[white][i])
            
    def clone(self):
        """Returns a clone of the board. Used for building the move tree"""
        return Board(self)

    def set_my_point(self,point, stone_color,count):
        """Sets a point from the perspective of the current player"""
        self.set_point(self.color,point,stone_color,count)
        
    def set_point(self,player_color,point, stone_color, count):
        """Sets a point's state for a player"""
        points = self.points[player_color]
        if count < 0: raise ValueError("Can't have negative stones on a point")
        if count == 0:
            points[point][constants.point_color] = None 
        else:
            points[point][constants.point_color] = stone_color
        points[point][constants.point_count]  = count
        
    
    
    def can_occupy_my_point(self,point):
        return self.can_occupy_point(self.color,self.color,point)
         
        
    def can_occupy_point(self,player_color, color, point):
        point = self.points[player_color][point]
        return (point[constants.point_color] == None          #if point is empty you can always occupy
            or point[constants.point_color] == color      #If point is your colour you can occupy
            or point[constants.point_count] == 1)        #If point contains 1 stone you can occupy
            
    
    def remove_my_stone(self, point):
        self.remove_stone(self.color,point)
    
    def remove_stone(self,player_color, point):
        point = self.points[player_color][point]
        if point[constants.point_count] == 0: raise ValueError("No stone(s) to remove on this point!")
        
        point[constants.point_count] -= 1
        
        if point[constants.point_count] == 0:
            point[constants.point_color] = None 
            
    def add_my_stone(self, point):
        return self.add_stone(self.color,point)
            
    def add_stone(self,player_color, point):
        self.points[player_color][point][constants.point_count] += 1
    
    def set_player(self, color):
        self.color = color
        
    def set_bar(self,player_color, count):
        self.bar[player_color] = count
        