import constants, board, move, movegenerator
import random

class RandomBot:
    def GetMove(self, board, roll):
        legal_moves = list(movegenerator.get_legal_moves_for_roll(board,roll)
        return random.choice(legal_moves).last_move
        
        
