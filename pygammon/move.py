import constants


class Move:
    def __init__(self, color=constants.white,half_moves=()):
        self.color = color
        self.half_moves = half_moves #Array of moves, either 2 or four
    
    def number_dice_played(self):
        """Fast method to determine number of playable dice"""
        half_moves = self.half_moves
        move_count = len(half_moves)
        if  move_count == 4:
            if half_moves[3].point != constants.move_pass:
                return 4
            
            if half_moves[2].point != constants.move_pass:
                return 3
                
            if half_moves[1].point != constants.move_pass:
                return 2
            
            if half_moves[0].point != constants.move_pass:
                return 1
            return 0
        elif move_count == 2:
            if half_moves[1].point != constants.move_pass:
                return 2
        
            if half_moves[0].point != constants.move_pass:
                return 1
            return 0
        else:
            raise ValueError("Invalid number of half_moves")
    
    def __str__(self):
        return f"{self.color} - {''.join(str(e) for e in self.half_moves)}"

class HalfMove:
    """Represents a backgammon move"""
    def __init__(self,point,roll):
        self.point = point    #The points selected to move for each roll, set to constants.move_pass to pass this roll
        self.roll =  roll     #the dice rolls that are associated with this move, usually how many points to move
    
    def __str__(self):
        if self.point == constants.move_pass:
            return f"(PASS roll:{self.roll}) "
        else:
            return f"(point:{self.point + 1} roll:{self.roll}) "
        
    def __repr__(self):
        return self.__str__()