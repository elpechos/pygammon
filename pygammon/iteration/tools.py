

def find_first_index(pred,iterable):
    """returns index of first matching item"""
    for i, val in enumerate(iterable):
        if(pred(val)):
            return i

def find_last_index(pred,iterable):
    """returns index of last matching item --todo: optimize for list"""
    result = None
    for i, val in enumerate(iterable):
        if(pred(val)):
            result = i
    return result            
    
            
def find_indexes(pred,iterable):
    """returns index of first matching item"""
    for i, val in enumerate(iterable):
        if(pred(val)):
            yield i        