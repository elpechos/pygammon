import iteration.tools as query
import constants, board, move, functools, itertools, dice
import time

def get_max_playable_dice(boards, maxValue = -1):
    """Returns the highest number of dice played in a list of boards, maxValue should be either 2 or 4 to short circuit"""
    maxDice = 0
    for board in boards:
        num_dice = board.last_move.number_dice_played()
        if num_dice == maxValue:
            return maxValue #Short circuit for performance.
        maxDice = max(maxDice,num_dice)
    return maxDice



def get_legal_moves(self):
    """Returns a list of all legal moves playable on this board"""
    for roll in dice.gen_rolls():
        yield from get_legal_moves_for_roll(self,roll)


def get_moves_for_roll(self, roll):
    """ returns a list of all moves for a roll, even illegal ones"""
    yield from get_boards_for_full_move(self,roll)
    if len(roll) == 2:
        yield from get_boards_for_full_move(self,roll[::-1])
        
        
def get_legal_moves_for_roll(self, roll):
    """This function returns legal moves for a roll, by removing illegal moves that break the passing rules"""
    boards = list(get_moves_for_roll(self,roll))
    if len(roll) > 2:
        #Order doesn't matter for quad moves but you have to play the most dice possible
        max_dice = get_max_playable_dice(boards,4)
        yield from (board for board in boards if board.last_move.number_dice_played() == max_dice)
    else: 
        #You have to play the most dice possible and highest dice first if only one can be played
        max_dice = get_max_playable_dice(boards,2)
        if max_dice == 1:
            #One roll or the other passes, yield the one that is higher
            bigroll = max(roll)
            yield from (board for board in boards if board.last_move[0].roll == bigroll)
        else:
            #Both or no dice can be played
            yield from (board for board in boards if board.last_move.number_dice_played() == max_dice)


def get_boards_for_full_move(self,roll, half_moves_played = ()):
    dice_roll  = roll[0]
    #Get all half_moves we can play with this roll.
    half_moves = _get_half_moves_for_dice(self, dice_roll)
    #for each of the half_moves play each one on the board
    for half_move in half_moves:
        board = self.clone()                        #clone the original board
        m = move.Move(self.color, (half_move,))     #play the move on it
        board.play_move(m)
        half_moves = half_moves_played + (half_move,)

        if len(roll) > 1:
            yield from get_boards_for_full_move(board,roll[1:], half_moves)
        else:
            board.last_move = move.Move(self.color, half_moves)
            #print(board.last_move)
            yield board
        

def _get_half_moves_for_dice(self,dice_roll):
    """Returns all possible half moves for a given dice value"""
    return _get_half_moves(self, dice_roll)
    

def _get_half_moves(self, start_dice = 1):
    #have to play an entering move (from the bar)
    if self.my_bar != 0:
        return _get_enter_moves(self,start_dice)
    
    #Have to play a bearing move
    if self.my_all_home:
        return _get_bear_moves(self, start_dice)
    
    #Just a regular old move
    return _get_regular_moves(self,start_dice)
    
        
def _get_enter_moves(self, roll):
        index = 24 - roll
        if self.can_occupy_my_point(index):
            yield move.HalfMove(index, roll)
        else:
            yield move.HalfMove(constants.move_pass, roll)
    

def _get_bear_moves(self, roll):
    #the player is permitted (and required) to remove a stone from the highest point on which one of his stones resides
    highest_point = query.find_last_index(lambda p: p[constants.point_color] == self.color,itertools.islice(self.my_points,roll))
    if highest_point != None:
        yield move.HalfMove(highest_point,roll)
    else:
        yield move.HalfMove(constants.move_pass,roll)

            
def _get_regular_moves(self,roll):
    #get list of all points where my stones are
    points_with_my_stones = list(query.find_indexes(lambda p: p[constants.point_color] == self.color, self.my_points))
    results = []
    #Get a list of dice rolls
    #It's not valid to bear off moves
    #bear moves are ones where a roll would move a point off the end of the board
    all_moves_excluding_bear = filter(lambda point: point - roll >= 0 , points_with_my_stones)
    
    #It's valid to move the stone to a point that's empty, or owned by you, etc
    valid_moves = filter(lambda point: self.can_occupy_my_point(point - roll),all_moves_excluding_bear)
    
    ##todo: if there are no moves for a dice roll should return moves.pass  
    moves = list(map(lambda point: move.HalfMove(point,roll),valid_moves))
        
    if len(moves) == 0:
        results.append(move.HalfMove(constants.move_pass,roll))

    results.extend(moves)
    
    return results
        
        