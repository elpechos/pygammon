from itertools import combinations_with_replacement

Rolls_cache = False

def gen_rolls():
    global Rolls_cache
    if not Rolls_cache:
        rolls = combinations_with_replacement(range(1, 7), 2)
        #In backgammon you get four moves if you roll doubles.
        Rolls_cache = tuple((x,)*4 if x == y else (x, y) for (x, y) in rolls)
    return  Rolls_cache
