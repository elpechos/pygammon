import unittest
import sys
sys.path.append('../pygammon/')
import board
import move
import random
from constants import *

class BoardTestcase(unittest.TestCase):
    def test_init_board(self):
        """Validates board initializes to the correct starting state"""
        b = board.Board()
        b.new_game(white)
        
        self.assertFalse(b.all_home[white],"All white stones aren't on home")
        self.assertFalse(b.all_home[black],"All black stones aren't on home")

        for color in colors:
            points = b.points[color]
            self.assertEqual(points[23][point_count], 2)
            self.assertEqual(points[23][point_color], color)
            
            self.assertEqual(points[12][point_count], 5)
            self.assertEqual(points[12][point_color], color)
            
            self.assertEqual(points[7][point_count], 3)
            self.assertEqual(points[7][point_color], color)
            
            self.assertEqual(points[5][point_count], 5)
            self.assertEqual(points[5][point_color], color)
            
    def test_play_move(self):
        """Plays a single move"""
        b = board.Board()
        b.new_game(white)

                    
        m = move.Move()
        m.color = white                 #white move
        m.half_moves =[move.HalfMove(roll = 1, point = 23)]
        b.play_move(m)
        
        points = b.points[white]
        self.assertEqual(points[23][point_count], 1, "A stone should have been removed")
        self.assertEqual(points[22][point_count], 1, "A stone should have been placed")
        self.assertEqual(points[22][point_color], white, "Point should be owned by white")  
    
    def test_capture_move(self):
        """Plays a capture move"""
        b = board.Board()
        b.new_game(white)

                    
        m = move.Move()
        m.color = white                 #white move
        m.half_moves =[move.HalfMove(roll = 1, point = 5)]
        b.play_move(m)
        
        capture_point = b.points[white][4]
        
        self.assertEqual(capture_point[point_count], 1, "A stone should have been placed")
        self.assertEqual(capture_point[point_color], white, "Point should have changed from None to white")
        
        
        b.color = black
        m = move.Move()
        m.color = black                 #black move
        m.half_moves =[move.HalfMove(roll = 4, point = 23)]
        b.play_move(m)
        
        self.assertEqual(b.bar[white], 1, "White should have one stone on the bar")
        self.assertEqual(capture_point[point_count], 1, "A stone should have been placed")
        self.assertEqual(capture_point[point_color], black, "Point should have changed from white to black")
        
        
    def test_bear(self):
        b = board.Board()
        b.set_point(white,0,white,1)
        b.set_point(white,1,white,2)
        b.set_point(white,2,white,3)
        b.set_player(white)
        b.initialize_board()
        points = b.my_points

        m = move.Move(white,[move.HalfMove(roll = 1, point = 0)])
        #Play a bearing move as white
        self.assertEqual(points[0][point_count], 1, "Stone should exist before bearing off")
        b.play_move(m)
        self.assertEqual(points[0][point_count], 0, "White should have beared off a stone")
        
        #you should be able to bear off a lower stone with a higher roll
        #If there is no higher stones we can bear off
        
        m = move.Move(white,[move.HalfMove(roll = 4, point = 2)])
        #Play a bearing move as white
        self.assertEqual(points[2][point_count], 3, "Stone should exist before bearing off")
        b.play_move(m)
        self.assertEqual(points[2][point_count], 2, "White should have beared off a stone")
        
    
    def test_bear_2(self):
        b = board.Board()
        b.set_point(white,5,white,1)
        b.set_player(white)
        b.initialize_board()
        points = b.my_points

        m = move.Move(white,[move.HalfMove(roll = 6, point = 5)])
        #Play a bearing move as white
        self.assertEqual(points[5][point_count], 1, "Stone should exist before bearing off")
        b.play_move(m)
        self.assertEqual(points[5][point_count], 0, "White should have beared off a stone")
        
        
        
    def test_must_bear_highest(self):
        b = board.Board()
        b.set_point(white,0,white,1)
        b.set_point(white,1,white,2)
        b.set_point(white,2,white,3)
        b.set_point(white,3,white,3)
        b.set_player(white)
        b.initialize_board()

        m = move.Move(white,[move.HalfMove(roll = 4, point = 2)])
        #Play a bearing move as white
        self.assertRaises(ValueError, b.play_move,m)
    
    def test_home(self):
        b = board.Board()
        
        b.set_point(white,0,white,1)
        b.set_point(white,1,white,2)
        b.set_point(white,2,white,3)
        b.set_player(white)
        b.initialize_board()

        self.assertTrue(b.my_all_home,"All white stones should be on home")
        
        
    def test_not_home(self):
        b = board.Board()
        b.set_point(white,0,white,1)
        b.set_point(white,1,white,2)
        b.set_point(white,2,white,3)
        b.set_point(white,7,white,3)
        b.set_player(white)
        b.initialize_board()
        self.assertFalse(b.my_all_home,"All white stones aren't on home")


    def test_enter(self):
        b = board.Board()
        b.set_point(white,18,black,1)
        b.set_bar(white,3)
        b.set_player(white)
        b.initialize_board()
        points = b.my_points
        self.assertEqual(b.my_bar, 3, "There should be three stones on the bar")
        m = move.Move(white,[move.HalfMove(roll = 4, point = bar)])
        b.play_move(m)
        self.assertEqual(b.my_bar, 2, "There should be two stones on the bar")
        self.assertEqual(points[20][point_count], 1, "White should have played a stone at 20")
        m = move.Move(white,[move.HalfMove(roll = 4, point = bar)])
        b.play_move(m)
        self.assertEqual(b.my_bar, 1, "There should be one stones on the bar")
        self.assertEqual(points[20][point_count], 2, "White should have played two stones at 20")


        m = move.Move(white,[move.HalfMove(roll = 6, point = bar)])
        self.assertEqual(points[18][point_color], black, "Point 18 should belong to black")
        self.assertEqual(points[18][point_count], 1, "There should be 1 stone at 18")
        b.play_move(m)
        self.assertEqual(b.my_bar, 0, "There should be no stones on the white bar")
        self.assertEqual(b.bar[black], 1, "There should be one stone on the black bar")
        
        self.assertEqual(points[18][point_color], white, "Point 18 should belong to wihte")
        self.assertEqual(points[18][point_count], 1, "There should be 1 stone at 18")
        
     
    def test_clone(self):
        b = board.Board()
        clone = b.clone()
        clone.set_point(white,0,white,5)
        self.assertEqual(clone.points[black][23][point_count],5,"Clone has damaged lists!")
        self.assertEqual(b.points[white][0][point_count],0,"Mutating clone shouldn't mutate original!")
    
    def test_clone_accurate(self):
        b = board.Board()

        for i in range(0, 24):
            b.set_point(white,0,random.choice([white,black]),random.randint(0,50))
            
        b.set_bar(white,random.randint(0,50))
        b.set_bar(black,random.randint(0,50))

        clone = b.clone()

        for i in range(0, 24):
            self.assertEqual(clone.points[white][i],b.points[white][i],"Clone should be identical to board!")

        self.assertEqual(clone.bar,b.bar,"Clone should be identical to board!")
        
