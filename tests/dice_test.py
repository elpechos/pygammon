import sys
sys.path.append('../pygammon/')
import unittest
import dice

class DicetestCase(unittest.TestCase):
    def test_rolls_count(self):
        rolls = list(dice.gen_rolls())
        self.assertEqual(len(rolls), 21, "Wrong number of dice rolls!")

    def test_quad_rolls(self):
        rolls = list(dice.gen_rolls())
        quads = [r for r in rolls if len(r) == 4]
        self.assertTrue(len(quads) == 6, "Should be six quad rolls")
        for q in quads:
            self.assertTrue(q[0] == q[1] == q[2] == q[3],"Quad rolls should all be equal")
            
            
            
    def test_dice_cache(self):
        rolls1 = dice.gen_rolls()
        rolls2 = dice.gen_rolls()
        self.assertTrue(rolls1 is rolls2, "Dice rolls should be cached")
        
            
            