import sys
sys.path.append('../pygammon/')
from constants import *
import unittest
import zorbisthash

class ZorbistHashTestCase(unittest.TestCase):
    def test_hash(self):
        hash = zorbisthash.ZorbistHash()
        self.assertEqual(0,hash.hash,"hash should start empty")
        hash.set(white,0,5)
        self.assertNotEqual(0,hash.hash,"hash should have a value")
        hash.set(white,0,5)
        self.assertEqual(0,hash.hash,"hash should be empty again")
        