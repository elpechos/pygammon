import sys
sys.path.append('../pygammon/')
import itertools, constants, unittest, board, movegenerator

def any_move(point, roll, moves):
    return any(x.point +1  == point and x.roll == roll for x in moves)
    

class MoveGeneratorTestCase(unittest.TestCase):
    
    def _get_half_moves_for_board(self,b):
        return [k for k in  itertools.chain.from_iterable(movegenerator._get_half_moves(b,roll) for roll in range(1,7))]

    def test_get_half_moves(self):
        b = board.Board()
        b.new_game(constants.white)
        moves = self._get_half_moves_for_board(b)
        
        self.assertEqual(len(moves),20,"Should be 20 half moves!")
        
        self.assertTrue(any_move(6,1,moves))
        self.assertTrue(any_move(8,1,moves))
        self.assertTrue(any_move(24,1,moves))
        
        self.assertTrue(any_move(6,2,moves))
        self.assertTrue(any_move(8,2,moves))
        self.assertTrue(any_move(13,2,moves))
        self.assertTrue(any_move(24,2,moves))
        
        self.assertTrue(any_move(6,3,moves))
        self.assertTrue(any_move(8,3,moves))
        self.assertTrue(any_move(13,3,moves))
        self.assertTrue(any_move(24,3,moves))
        
        self.assertTrue(any_move(6,4,moves))
        self.assertTrue(any_move(8,4,moves))
        self.assertTrue(any_move(13,4,moves))
        self.assertTrue(any_move(24,4,moves))
    
        self.assertFalse(any_move(6,5,moves))
        self.assertTrue(any_move(8,5,moves))
        self.assertTrue(any_move(13,5,moves))
        self.assertFalse(any_move(24,5,moves))
        
        self.assertFalse(any_move(6,6,moves))
        self.assertTrue(any_move(8,6,moves))
        self.assertTrue(any_move(13,6,moves))
        self.assertTrue(any_move(24,6,moves))
        
        
    def test_get_half_moves_capture(self):
        b = board.Board()
        b.set_point(constants.white,23,constants.white,1)
        b.set_point(constants.white,22,constants.black,1)
        b.set_player(constants.white)
        b.initialize_board()
        moves = self._get_half_moves_for_board(b)
        self.assertEqual(len(moves),6,"Should be 6 half moves!")
        for roll in range(1,7):
            self.assertTrue(any_move(24,roll,moves))
            
            
    def test_get_half_moves_capture2(self):
        b = board.Board()
        b.set_point(constants.white,23,constants.white,1)
        b.set_point(constants.white,22,constants.black,2)
        b.set_player(constants.white)
        b.initialize_board()
        moves = self._get_half_moves_for_board(b)
        self.assertEqual(len(moves),6,"Should be 6 half moves!")
        self.assertTrue(any_move(constants.move_pass + 1,1,moves),"Roll 1 should be a pass")

        for roll in range(2,7):
            self.assertTrue(any_move(24,roll,moves))
            

    def test_get_half_moves_bear(self):
        b = board.Board()
        
        b.set_point(constants.white,2,constants.white,1)
        b.set_point(constants.white,3,constants.white,2)
        b.set_point(constants.white,4,constants.white,3)
        b.set_player(constants.white)
        b.initialize_board()
        moves = self._get_half_moves_for_board(b)
        self.assertEqual(len(moves),6,"Should be 6 half moves!")
        self.assertTrue(any_move(constants.move_pass + 1,1,moves))
        self.assertTrue(any_move(constants.move_pass + 1,2,moves))
        self.assertTrue(any_move(3,3,moves))
        self.assertTrue(any_move(4,4,moves))
        self.assertTrue(any_move(5,5,moves))
        self.assertTrue(any_move(5,6,moves))
        
    def test_get_half_moves_enter(self):
        b = board.Board()
        
        b.set_point(constants.white,21,constants.black,1)
        b.set_point(constants.white,20,constants.black,2)
        b.set_point(constants.white,19,constants.black,3)
        b.set_player(constants.white)
        b.set_bar(constants.white,2)
        moves = self._get_half_moves_for_board(b)
        self.assertEqual(len(moves),6,"Should be 6 half moves!")
        self.assertTrue(any_move(24,1,moves))
        self.assertTrue(any_move(23,2,moves))
        self.assertTrue(any_move(22,3,moves))
        self.assertTrue(any_move(constants.move_pass + 1,4,moves))
        self.assertTrue(any_move(constants.move_pass + 1,5,moves))
        self.assertTrue(any_move(19,6,moves))
        
    
    def test_get_legal_moves(self):
        b = board.Board()
        b.new_game(constants.white)
        results = list(movegenerator.get_legal_moves(b))
        self.assertEqual(len(results),2195, "should be x top level moves!")
